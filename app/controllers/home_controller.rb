class HomeController < ActionController::Base
  	def index
  		$gvar = 0
  		jmljawaban = ('0'..(JAWABAN.length - 1).to_s).to_a.shuffle.first(1).join
  		@arraysoal = jmljawaban.to_i
  		@x = JAWABAN[jmljawaban.to_i]
  		y = (@x.length - 1 ).to_s
  		x2 = ""
  		loop do
  			@x1 = ('0'..y).to_a.shuffle.first(@x.length.to_i).join 
			x2 = ""
			for i in 0..(@x.length-1)
				x2 = x2 + @x[@x1[i].to_i]
			end  
		  	break if (x2.to_s != @x.to_s)
		end 
	  	@x3 = x2
  	end
  	def cekdata
  		jwb = params[:jwbnya]
  		soalaaray = params[:arraysoal]

  		if jwb == JAWABAN[soalaaray.to_i]
  			@x = $gvar.to_i
	  		@x += 1
	  		$gvar = @x
	  		jmljawaban = 0
	  		loop do
	  			jmljawaban = ('0'..(JAWABAN.length - 1).to_s).to_a.shuffle.first(1).join
	  			break if (soalaaray.to_i != jmljawaban.to_i)
	  		end 
	  		@arraysoal = jmljawaban.to_i
	  		@x = JAWABAN[jmljawaban.to_i]
	  		y = (@x.length - 1 ).to_s
	  		x2 = ""
	  		loop do
	  			@x1 = ('0'..y).to_a.shuffle.first(@x.length.to_i).join 
				x2 = ""
				for i in 0..(@x.length-1)
					x2 = x2 + @x[@x1[i].to_i]
				end  
			  	break if (x2.to_s != @x.to_s)
			end 
		  	@x3 = x2
  			# render plain: "Jawaban Anda Benar, Poin Anda : " + $gvar.to_s
  			render json: [{pesan:"Jawaban Anda Benar",poin:$gvar.to_s, newsoal:@x3.to_s, newarraysoal:@arraysoal.to_s}]
  		else
  			render json: [{pesan:"Jawaban Anda Salah",poin:"0"}]
  		end
  	end
  	def cekpoin
  		render plain: $gvar
  	end
end
